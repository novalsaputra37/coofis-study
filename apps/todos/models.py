from django.db import models
from apps.common.models import TimeStampedUUIDModel
from django.utils.translation import gettext_lazy as _

class Todo(TimeStampedUUIDModel):
    class Priority(models.TextChoices):
        NORMAL = "normal", _("normal")
        URGENT = "urgent", _("urgent")

    title = models.CharField(verbose_name=_("title"), max_length=255)
    description = models.TextField(verbose_name=_("description"))
    priority = models.CharField(
        verbose_name=_("priority"),
        choices=Priority.choices,
        default=Priority.NORMAL,
        max_length=15,
    )
    creator = models.ForeignKey(
        "accounts.User",
        on_delete=models.CASCADE,
        verbose_name=_('creator'),
        related_name='creator_todo')
    
    class Meta:
        verbose_name = 'todo'
        verbose_name_plural = 'todos'
        db_table = "todos_todo"

    def __str__(self):
        return self.title

class Attachment(TimeStampedUUIDModel):
    todo = models.ForeignKey(
        "todos.Todo",
        on_delete=models.CASCADE,
        blank=True, null=True,
        verbose_name=_('todo'),
        related_name='attachments')
    name = models.CharField(max_length=100, verbose_name='Name', blank=True, null=True)
    description = models.TextField(verbose_name=_('description'), blank=True, null=True)
    file = models.FileField(
        upload_to='attachments/',
        verbose_name=_('file'),
        blank=True,
        null=True)

    class Meta:
        verbose_name = 'attachment'
        verbose_name_plural = 'attachments'
        db_table = "todos_attachment"
    
    def __str__(self):
        return self.name
