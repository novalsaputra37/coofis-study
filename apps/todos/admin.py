from django.contrib import admin
from apps.todos.models import Todo, Attachment

admin.site.register(Todo)
admin.site.register(Attachment)