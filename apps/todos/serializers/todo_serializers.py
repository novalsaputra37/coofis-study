import base64
from rest_framework import serializers
from apps.todos.models import Attachment, Todo
from apps.todos.serializers.attachment_serializers import AttachmentSerializer
from django.utils import timezone

class TodoSerializers(serializers.ModelSerializer):
    attachments = AttachmentSerializer(many=True, required=False)
    hf_attachments = serializers.ListField(
        child=serializers.CharField(max_length=100), required=False
    )
    
    class Meta:
        model = Todo
        fields = [
            "id",
            "title",
            "description",
            "priority",
            "hf_attachments",
            "attachments"
        ]

    def create(self, validated_data):
        user = self.context.get("user")
        hf_attachments = validated_data.pop("hf_attachments")
        attachments = validated_data.pop("attachments")
        
        validated_data['creator'] = user
        obj_todo = Todo.objects.create(**validated_data)

        if hf_attachments:
            Attachment.objects.filter(id__in=hf_attachments).update(todo=obj_todo)
        if attachments:
            serializer = AttachmentSerializer(data=attachments, many=True, context={"obj_todo": obj_todo})
            serializer.is_valid()
            serializer.save()

        return obj_todo
    
    def update(self, instance, validated_data):
        get_hf_attachments = validated_data.get("hf_attachments")
        get_attachments = validated_data.get("attachments")
        hf_attachments = validated_data.pop("hf_attachments") if get_hf_attachments else None
        attachments = validated_data.pop("attachments") if get_attachments else None

        instance = super(TodoSerializers, self).update(instance, validated_data)
        
        if hf_attachments:
            attch_old = Attachment.objects.filter(todo=instance)
            attch_now = attch_old.exclude(id__in=hf_attachments)
            attch_now.delete()
            Attachment.objects.filter(id__in=hf_attachments).update(todo=instance)
        if attachments:
            serializer = AttachmentSerializer(data=attachments, many=True, context={"obj_todo": instance})
            serializer.is_valid()
            serializer.save()

        return instance





    
class TodoListSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()

    class Meta:
        model = Todo
        fields = [
            "id",
            "title",
            "description",
            "priority",
            "created_at"
        ]
    
    def get_created_at(self, obj):
        date = timezone.localtime(obj.created_at)
        formatted_date = date.strftime("%d %B %Y %H:%M:%S")
        return formatted_date
    
class TodoDestroySerializer(serializers.Serializer):
    id_todos = serializers.ListField(
        child=serializers.CharField(max_length=100)
    )

    def destroy(self, validated_date):
        get_id_todos = validated_date.get("id_todos")
        Todo.objects.filter(id__in=get_id_todos).update(is_delete=True)